<?php
  session_start();
  require_once("../protected/dbconnection.config.php");
  include"../include/cookie.inc.php";
  include"../include/func.inc.php";

  blockcheck();

  if (!isset($_SESSION["login"])) {
    header('Location: signup.controller.php');
  }
  else {
    $name = $_SESSION["login"];
    $profile = profile();
    include"../view/profile.view.php";
  }
?>
