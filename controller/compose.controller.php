<?php
  session_start();
  require_once("../protected/dbconnection.config.php");
  include"../include/cookie.inc.php";
  include"../include/func.inc.php";

  blockcheck();

  if (isset($_SESSION["login"])) {
    if (isset($_POST["send"])) {
      $message_error = compose();
    }
    $name = $_SESSION["login"];
    include"../view/compose.view.php";
  }
  else {
    header('Location: inbox.controller.php');
  }
?>
