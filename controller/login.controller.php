<?php
  session_start();
  require_once("../protected/dbconnection.config.php");
  include"../include/cookie.inc.php";
  include"../include/func.inc.php";

  blockcheck();

  if (!isset($_SESSION["login"])) {
    $useragent = $_SERVER["HTTP_USER_AGENT"];
    $ip = $_SERVER["REMOTE_ADDR"];
    querySQL("INSERT INTO `block`(`ipuser`, `attemp`, `blocktime`) VALUES ('$ip',0,'')");
    if (isset($_POST["login"])) {
      $message_error = login();
    }
    include"../view/login.view.php";
  }
  else {
    header('Location: inbox.controller.php');
  }

?>
