<?php
  session_start();
  require_once("../protected/dbconnection.config.php");
  include"../include/cookie.inc.php";
  include"../include/func.inc.php";

  blockcheck();

  if (!isset($_SESSION["login"])) {
    header('Location: signup.controller.php');
  }
  else {
    $name = $_SESSION["login"];
    if (isset($_POST["filter"])) {
      $mail = fliterFile();
    }
    else {
      $mail = inbox();
    }
    include"../view/inbox.view.php";
  }
?>
