<?php
  session_start();
  require_once("../protected/dbconnection.config.php");
  include"../include/cookie.inc.php";
  include"../include/func.inc.php";

  blockcheck();

  if (isset($_SESSION["login"])) {
    $mail = showinboxmail();
    $name = $_SESSION["login"];
    include"../view/showmailinbox.view.php";
  }
  else {
    header('Location: inbox.controller.php');
  }
?>
