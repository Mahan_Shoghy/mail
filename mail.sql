-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2019 at 04:42 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mail`
--

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE `block` (
  `ipuser` int(20) NOT NULL,
  `attemp` int(1) NOT NULL,
  `blocktime` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `block`
--

INSERT INTO `block` (`ipuser`, `attemp`, `blocktime`) VALUES
(0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `darft`
--

CREATE TABLE `darft` (
  `iddarft` int(11) NOT NULL,
  `users_has_mail_users_idusers` int(11) NOT NULL,
  `users_has_mail_mail_idmail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `darft`
--

INSERT INTO `darft` (`iddarft`, `users_has_mail_users_idusers`, `users_has_mail_mail_idmail`) VALUES
(1, 1, 2),
(3, 1, 13),
(4, 1, 14),
(5, 2, 19),
(6, 2, 20);

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE TABLE `mail` (
  `idmail` int(11) NOT NULL,
  `subject` varchar(42) DEFAULT NULL,
  `text` text,
  `file` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `mail_idgiver` int(20) NOT NULL,
  `seen` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mail`
--

INSERT INTO `mail` (`idmail`, `subject`, `text`, `file`, `date`, `time`, `mail_idgiver`, `seen`) VALUES
(2, 'asdasdasd', 'asdasdasdasdasdas\r\ndasdasdasdasd\r\nasdasdasdasd', '../file/mail.sql', '2019-09-30', '21:43:55', 0, 1),
(3, 'sag', 'asd;lskdl;askd\r\njsldkfjl;sfdhl\r\nlksjdlksd', '0', '2019-09-27', '14:54:59', 2, 1),
(4, 'csadad', 'awddadawdddddddddddddddddddddddddddddddddddd', '0', '2019-10-21', '19:33:32', 1, 1),
(6, 'work', 'sakdmsalkdm\r\n\r\n\r\naskdalkdm\r\nas;d;kad', '../file/mail.sql', '2019-10-15', '11:20:40', 0, 1),
(7, 'salam', 'sadasdasdad\r\nhow are you', '0', '2019-10-03', '11:19:21', 0, 1),
(13, 'dasd', 'asdasdasd', '0', '2019-10-03', '19:06:09', 0, 0),
(14, 'asdasd', 'asdasdadsa', '0', '2019-10-03', '19:20:52', 0, 0),
(15, 'Dotarafe', 'Salam aghayyyyyoooooooon ;)', '../file/mail.mwb', '2019-10-03', '20:09:10', 0, 1),
(16, 'hardo', 'goorrrrrrrre khooodet!! martikeee!!!', '../file/20141108_133818.jpg', '2019-10-03', '20:12:25', 0, 0),
(17, 'draft bere', 'inahaaaaaashsshshhs', '0', '2019-10-03', '20:13:52', 0, 1),
(18, 'bara mahan', 'cheeeeetorrrrry khareeeeeeee!!! :D', '../file/20141113_093226.jpg', '2019-10-03', '20:14:37', 0, 1),
(19, 'dsgsgd', 'sdrgdhdhj', '0', '2019-10-04', '07:58:03', 0, 0),
(20, 'Aree', 'Bebiiiiin', '0', '2019-10-04', '08:01:58', 0, 0),
(29, 'har2', 'Salaaaam aghayoon', '0', '2019-10-05', '18:10:11', 2, 0),
(30, 'har2', 'Salaaaam aghayoon', '0', '2019-10-05', '18:10:11', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idusers` int(11) NOT NULL,
  `users_username` varchar(45) DEFAULT NULL,
  `users_password` varchar(100) DEFAULT NULL,
  `users_email` varchar(100) DEFAULT NULL,
  `signither` varchar(20) DEFAULT NULL,
  `attemp` int(1) NOT NULL,
  `timeblock` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `users_username`, `users_password`, `users_email`, `signither`, `attemp`, `timeblock`) VALUES
(1, 'Extremedbd', '746a85f732c7511ea8babbadcd917d7c', 'mahan@yahoo.com', NULL, 0, '00:00:00'),
(2, 'Mahan123', '746a85f732c7511ea8babbadcd917d7c', 'mahan_1414@yahoo.com', NULL, 0, '00:00:00'),
(3, 'Shayan456', '746a85f732c7511ea8babbadcd917d7c', 'shayan123yahoo.com', NULL, 0, '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_has_mail`
--

CREATE TABLE `users_has_mail` (
  `users_idusers` int(11) NOT NULL,
  `mail_idmail` int(11) NOT NULL,
  `idgiver` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_has_mail`
--

INSERT INTO `users_has_mail` (`users_idusers`, `mail_idmail`, `idgiver`) VALUES
(1, 2, 0),
(1, 3, 2),
(1, 13, 0),
(1, 14, 0),
(1, 29, 2),
(1, 30, 3),
(2, 4, 1),
(2, 7, 1),
(2, 19, 0),
(2, 20, 0),
(3, 15, 2),
(3, 18, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`ipuser`);

--
-- Indexes for table `darft`
--
ALTER TABLE `darft`
  ADD PRIMARY KEY (`iddarft`,`users_has_mail_users_idusers`,`users_has_mail_mail_idmail`),
  ADD KEY `fk_darft_users_has_mail1_idx` (`users_has_mail_users_idusers`,`users_has_mail_mail_idmail`);

--
-- Indexes for table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`idmail`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- Indexes for table `users_has_mail`
--
ALTER TABLE `users_has_mail`
  ADD PRIMARY KEY (`users_idusers`,`mail_idmail`),
  ADD KEY `fk_users_has_mail_mail1_idx` (`mail_idmail`),
  ADD KEY `fk_users_has_mail_users_idx` (`users_idusers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `darft`
--
ALTER TABLE `darft`
  MODIFY `iddarft` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mail`
--
ALTER TABLE `mail`
  MODIFY `idmail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `darft`
--
ALTER TABLE `darft`
  ADD CONSTRAINT `fk_darft_users_has_mail1` FOREIGN KEY (`users_has_mail_users_idusers`,`users_has_mail_mail_idmail`) REFERENCES `users_has_mail` (`users_idusers`, `mail_idmail`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_has_mail`
--
ALTER TABLE `users_has_mail`
  ADD CONSTRAINT `fk_users_has_mail_mail1` FOREIGN KEY (`mail_idmail`) REFERENCES `mail` (`idmail`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_mail_users` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
