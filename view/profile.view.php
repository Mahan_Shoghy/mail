<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <style>
        nav{
            margin-bottom: 30px;
        }
        a , a:link , a:visited  , a:hover  , a:active {
            text-decoration: none;
            color: #000;
        }
        li:hover{
            background-color: lightgreen;
        }
        table tr{
            cursor:pointer;
        }
        tbody tr:hover{
            box-shadow: 3px 3px 12px grey;

        }
    </style>
</head>
<body>
<?php
    include_once 'navbar.view.php';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 ">
            <?php
            include 'menu.view.php';
            ?>
        </div>
        <div class="col-lg-9 profile_box">
          <p style="margin-top:90px; font-size: 40px;">
            <label>Username: </label> <?= $profile[0][0] ?>
          </p>
          <p style="margin-top:50px; font-size: 40px;">
            <label>Email: </label> <?= $profile[0][1] ?>
          </p>
        </div>
    </div>
</div>

</body>
</html>
