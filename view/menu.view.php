<ul class="list-group text-center">
    <div class="list-group-item"><h3><?=$name ?></h3></div>
    <a href="compose.controller.php"><li class="list-group-item">Compose</li></a>
    <a href="inbox.controller.php"><li class="list-group-item">Inbox</li></a>
    <a href="draft.controller.php"><li class="list-group-item">Drafts</li></a>
    <a href="sent.controller.php"><li class="list-group-item">Sent</li></a>
    <a href="profile.controller.php"><li class="list-group-item">Profile</li></a>
    <a href="logout.controller.php"><li class="list-group-item">Logout</li></a>
</ul>
