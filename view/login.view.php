<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="wrapper">
      <div class="box">
        <div class="content_box">
          <form action="" method="post" enctype="application/x-www-form-urlencoded">
            <div class="form-group">
              <label for="exampleInputUsername">Username</label>
              <input type="text" name="username" class="form-control" id="exampleInputUsername" aria-describedby="emailHelp" placeholder="Enter email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
            </div>
            <input type="checkbox" name="rememberme"> Remember Me
            <br><br>
            <input type="submit" class="btn btn-primary" name="login" value="Login">
            <p>
              <?php
                if (isset($message_error)) {
                  echo "<span class='alert alert-danger' role='alert'>$message_error</span><br>";
                }
              ?>
            </p>
          </form>
        </div>
        <p class="alert alert-dark" role="alert" style="margin-top:20px;">
          <label>Dont't have an account? <a href="signup.controller.php">Sign Up</a></label>
        </p>
      </div>
    </div>
  </body>
</html>
