<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <style>
        nav{
            margin-bottom: 30px;
        }
        a , a:link  , a:hover  , a:active {
            text-decoration: none;
            color: #000;
        }
        a:visited {
          opacity: 25%;
        }
        li:hover{
            background-color: lightgreen;
        }
        table tr{
            cursor:pointer;
        }
        tbody tr:hover{
            box-shadow: 3px 3px 12px grey;

        }
    </style>
</head>
<body>
<?php
include_once "navbar.view.php";
?>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 ">
                    <?php
                    include 'menu.view.php';
                    ?>
                </div>
                <div class="col-lg-9">
                    <h5>From: <?= $mail[0][5] ?> </h5>
                    <h1 class="display-4"><?= $mail[0][0] ?></h1>
                    <p class="lead"><?= $mail[0][2] ?></p>
                    <?php
                        if ($mail[0][4] !== '0'){
                            echo '<a href="'.$mail[0][4].'">attached file</a>';
                        }
                    ?>
                    <p class="lead"><?= $mail[0][1] ?></p><p class="lead"><?= $mail[0][3] ?></p>
                </div>
            </div>
        </div>
</body>
</html>
