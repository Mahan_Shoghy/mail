<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <style>
        nav{
            margin-bottom: 30px;
        }
        a , a:link , a:visited  , a:hover  , a:active {
            text-decoration: none;
            color: #000;
        }
        li:hover{
            background-color: lightgreen;
        }
        table tr{
            cursor:pointer;
        }
        tbody tr:hover{
            box-shadow: 3px 3px 12px grey;

        }
    </style>
</head>
<body>
<?php
    include_once 'navbar.view.php';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 ">
            <?php
            include 'menu.view.php';
            ?>
        </div>
        <div class="col-lg-9">
          <form class="" action="" method="post">
            <button type="submit" name="filter" class="btn btn-warning">Fliter Attached File Mails</button>
          </form>
            <table class="table table-striped" id="inbox">
                <thead>
                <th scope="col">#</th>
                <th scope="col">From</th>
                <th scope="col">Subject</th>
                <th scope="col">Date</th>
                </thead>
            </table>
            <div class="inbox_box">
              <?php
                foreach ($mail as $key => $value) {
                  $key += 1;
                  echo '<div style="height:5px; display:block"><a href="showmailinbox.controller.php?idmail='.$value[3].'">
                  <div style="display:inline-block; float:left; width:12%; margin-left:12px;">'.$key.'</div>
                  <div style="display:inline-block; float:left; width:30%;">'.$value[2].'</div>
                  <div style="display:inline-block; float:left; width:31%;">'.$value[0].'</div>
                  <div style="display:inline-block; float:left; width:15%;">'.$value[1].'</div></a></div><button type="button" name="delete"><a href="delete.controller.php?idmail='.$value[3].'">Delete</a></button><br><hr>';
                }
              ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>
