<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <style>
        nav{
            margin-bottom: 30px;
        }
        a , a:link , a:visited  , a:hover  , a:active {
            text-decoration: none;
            color: #000;
        }
        li:hover{
            background-color: lightgreen;
        }
        table tr{
            cursor:pointer;
        }
        tbody tr:hover{
            box-shadow: 3px 3px 12px grey;

        }
    </style>
</head>
<body>
<?php
    include_once 'navbar.view.php';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 ">
            <?php
            include 'menu.view.php';
            ?>
        </div>
        <div class="col-lg-9">
            <table class="table table-striped" id="inbox">
                <thead>
                <th scope="col">#</th>
                <th scope="col">Subject</th>
                <th scope="col">Date</th>
                </thead>
            </table>
            <div class="inbox_box">
              <?php
                if ($mail != null) {
                  foreach ($mail as $key => $value) {
                    $key += 1;
                    echo '<div style="height:5px; display:block"><a href="showmailsearch.controller.php?idmail='.$value[2].'">
                    <div style="display:inline-block; float:left; width:20%; margin-left:12px;">'.$key.'</div>
                    <div style="display:inline-block; float:left; width:44%;">'.$value[0].'</div>
                    <div style="display:inline-block; float:left; width:15%;">'.$value[1].'</div></a></div><br><hr>';
                  }
                }
                else {
                  echo "None";
                }
              ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>
