<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="wrapper">
      <div class="box">
        <div class="content_box">
          <form action="" method="post" enctype="application/x-www-form-urlencoded">
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputUsername">Username</label>
              <input type="text" name="username" class="form-control" id="exampleInputUsername" aria-describedby="emailHelp" placeholder="Enter email" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
            </div>
            <input type="submit" class="btn btn-primary" name="signup" value="Sign up">
            <p>
              <?php
                // die(var_dump($message_error));
                if (isset($message_error)) {
                  foreach ($message_error[0] as $value) {
                    if ($value != null) {
                      echo "<span class='alert alert-danger' role='alert'>$value</span><br>";
                    }
                  }
                }
              ?>
            </p>
          </form>
        </div>
        <p class="alert alert-dark" role="alert" style="margin-top:20px;">
          <label>Already have an account? <a href="login.controller.php">Login</a></label>
        </p>
      </div>
    </div>
  </body>
</html>
