<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <style>
        nav{
            margin-bottom: 30px;
        }
        a , a:link , a:visited  , a:hover  , a:active {
            text-decoration: none;
            color: #000;
        }
        li:hover{
            background-color: lightgreen;
        }
        table tr{
            cursor:pointer;
        }
        tbody tr:hover{
            box-shadow: 3px 3px 12px grey;

        }
    </style>
</head>
<body>
<?php
    include_once "navbar.view.php"
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 ">
            <?php
            include "menu.view.php";
            ?>
        </div>
        <div class="col-lg-9">
          <div class="alert alert-warning" role="alert">
            For send to multi users use ( & ) between usernames!
          </div>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md mb-3">
                    <label for="to">Recipients:</label>
                    <input type="text" name="recipients" class="form-control" id="to" placeholder="">
                </div>
                <div class="col-md mb-3">
                    <label for="Subject">Subject:</label>
                    <input type="text" name="subject" class="form-control" id="Subject" value="<?= $mail[0][0] ?>" placeholder="">
                </div>
                <div class="col-md mb-3">
                    <label for="Message">Message:</label>
                    <textarea name="mail" id="Message" class="form-control" rows="10" placeholder="Message here..."><?= $mail[0][1] ?></textarea>
                </div>
                <div class="col-md mb-3">
                    <label for="File">Attached File:</label>
                    <input type="file" id="File" name="file" id="attach" >
                </div>
                <div class="row">
                  <input class="btn btn-success btn-lg col-md" style="float:right;" type="submit" name="send" value="Send">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <?php
            if (isset($message_error[0])) {
              foreach ($message_error[0] as $key => $value) {
                echo "<span class='alert alert-danger' role='alert' style='margin-left:15px;'>$value</span><br><br>";
              }
            }
            if (isset($message_error[1])) {
              echo "<span div class='alert alert-success' role='alert' style='margin-left:15px;'>$message_error[1]</span><br>";
            }
          ?>
      </div>
    </div>
</div>
</body>
</html>
