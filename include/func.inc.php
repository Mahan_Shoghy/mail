<?php

# validate name lastname city
function validatenames($value,$name)
{
  if (!preg_match('/^[A-Za-z0-9.\s_-]+$/',$value)) {
    $message_error = "*".$name." must contain letters and numbers";
    return $message_error;
  }
}

# function for validate some inputs
function validate($str) {
  return trim(htmlspecialchars($str));
}

# function for search text in a value and if true show your message
function boolSearchInInputs($regex,$value,$msg)
{
  if (preg_match('/'.$regex.'/',$value)) {
    $message_error = $msg;
    return $message_error;
  }
}

# function for replace text in a value
function replaceInInputs($reg_where,$value,$text)
{
  return preg_replace('/'.$reg_where.'/',$text,$value);
}


# function for run sql easy
function querySQL($sql)
{
  $connection = mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
  $sql = $sql;
  $spc = mysqli_query($connection,$sql); // returns a mysql_result object
  mysqli_close($connection);
  return $spc;
}

# get all details of users from database
function getAllUsersFromDatabase()
{
  $user = querySQL("select * from users");
  $resource = array();
  foreach ($user as $key => $value) { // put values of array in another array with username key
    $resource[$value['users_username']] = $value;
  }
  return $resource;
}

# better than default set cookie
function mySetCookie($name,$value,$expirytime)
{
  setcookie($name,$value,$expirytime,null,null,null,true);
}

# check inputs if they empty show message
function checkEmptyInputs($value,$err_msg)
{
  if (empty($value)) {
    if (isset($err_msg)) {
      $message_error = $err_msg;
      return $message_error;
    }
  }
}

# check user block or not
function blockcheck()
{
  $ip = $_SERVER["REMOTE_ADDR"];
  $currenttime = time();
  $attemp = mysqli_fetch_row(querySQL("select attemp from block where '$ip' = ipuser"));
  if ($attemp[0] >= 3) {
    querySQL("UPDATE `block` SET `blocktime`='$currenttime' WHERE '$ip' = ipuser");
    header('Location: block.controller.php');
  }
}

# check user time limit block
function block()
{
  $ip = $_SERVER["REMOTE_ADDR"];
  $time = mysqli_fetch_all(querySQL("select blocktime from block where '$ip' = ipuser"));
  $currenttime = time();

  if (($currenttime - $time[0][0]) >= 3600) {
    querySQL("UPDATE `block` SET `attemp`= 0,`blocktime`='' '$ip' = ipuser");
    header('Location: ../index.php');
  }
  else {
    include"../view/block.view.php";
  }
}

# function for validate all signup inputs
function valdatesignup()
{
  if (!empty($_POST["email"]) && !empty($_POST["username"]) && !empty($_POST["password"]) ) {

    $email = validate($_POST['email']);
    $str_pass = ('/^(?=\P{Ll}*\p{Ll})(?=\P{Lu}*\p{Lu})(?=\P{N}*\p{N})(?=[\p{L}\p{N}]*[^\p{L}\p{N}])[\s\S]{8,}$/'); // regex for password
    $str_names = '/[a-zA-Z]*/';

    $source_vali = array("Email" => $_POST["email"] , "Username" => $_POST["username"]);
    foreach ($source_vali as $name => $value) {
      $message_error[] = boolSearchInInputs($value,$_POST["password"],"*Password not be contain with your $name");
    }

    $message_error[] = validatenames($source_vali["Username"],"Username");

    if (!preg_match($str_pass, $_POST['password'])) {
      $message_error[] = "*Invaild Password ( Contain letters ( upper,lower ) , Number , Special Char )";
    }

    if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
      $message_error[] = "*Invalid Email ( Contain Letters , Numbers , @ & .domain )";
    }

    $file = fopen("../blcklistPasswords.txt", "r");
    $a = 0;
    $resource = array();
    while ($a = fgets($file)) {
      $resource[] = trim($a);
        if($_POST['password']===$resource){
          $message_error[] = "*Password To Easy";
        }
    }
    fclose($file);

  }
  else {
    $message_error[] = "*Please Fill All Fields";
  }
  return $message_error;
}

# do all signup works
function signup()
{
  if (!empty(valdatesignup())) {
    $message_error[] = valdatesignup();
  }
  else {
    $password = $_POST["password"];
    $password = md5($password);

    $source = array("Email" => $_POST["email"],"Username" => $_POST["username"]);

    foreach ($source as $key => $variable) {
      $replaced = replaceInInputs('[#$%^*(),{}<>]',$variable,"");
      if ($variable !== $replaced) {
        $source[$key] = $replaced;
      }
    }

    $source["Password"] = $password;

    $resource = getAllUsersFromDatabase();

    $mail = querySQL("select email from users");
    $resource_mail = array();
    foreach ($mail as $key => $value) {
      $resource_mail[$value['email']] = $value;
    }

    if (isset($resource[$source['Username']]) || isset($resource_mail[$source['Email']])) {
      if (isset($resource[$source['Username']])) {
        $message_error[] = "*This username already taken. Please enter another one";
      }
      elseif (isset($resource_mail[$source['Email']])) {
        $message_error[] = "*This email in used. Please enter another one";
      }
    }
    else {

      $email = $source['Email'];
      $username = $source['Username'];
      $password = $source['Password'];

      querySQL("INSERT INTO `users`(`idusers`, `users_username`, `users_password`, `users_email`) VALUES ('','$username','$password','$email')");
      header('Location: login.controller.php');
    }
  }
  return $message_error;
}

# do all login works
function login()
{
  $useragent = $_SERVER["HTTP_USER_AGENT"];
  $ip = $_SERVER["REMOTE_ADDR"];

  if (!empty($_POST["password"]) && !empty($_POST["username"])) {
    $password = $_POST["password"];
    $password = md5($password);

    $source = array($_POST["username"],$password);

    $resource = getAllUsersFromDatabase();

    if (!isset($resource[$source[0]])) {
      $message_error = "*Invaild Username or Password";
      querySQL("UPDATE `block` SET `attemp`= (attemp + 1) WHERE '$ip' = ipuser");
      return $message_error;
    }
    if (isset($resource[$source[0]]) && !in_array($source[1],$resource[$source[0]])) {
      $message_error = "*Invaild Username or Password";
	    querySQL("UPDATE `block` SET `attemp`= (attemp + 1) WHERE '$ip' = ipuser");
      return $message_error;
    }

    if (!isset($_POST['rememberme'])) {
      $_SESSION["login"] = $source[0];
    }
    else {
      $_SESSION["login"] = $source[0];
      $characters = '0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+';
        $randomString = '';
        for ($i = 0; $i < 20; $i++) {
            $randomString .= $characters[rand(0,47)];
        }

      $expirytime = time()+60*60*24;
      mySetCookie('username',$randomString,$expirytime);

      querySQL("UPDATE users SET signither='$randomString' WHERE '$source[0]' = users_username");
    }

    header('Location: mail.controller.php');

  }
  else {
    $message_error = "*Please Fill All Fields";
    return $message_error;
  }
}

# send message
function send():array
{

  $message_error = array();

  if (checkEmptyInputs($_POST['subject'],"!Please Enter Subject") != null) {
    $message_error[0][0] = checkEmptyInputs($_POST['subject'],"!Please Enter Subject");
  }

  if (checkEmptyInputs($_POST['mail'],"!Please write your message") != null) {
    $message_error[0][1] = checkEmptyInputs($_POST['mail'],"!Please write your message");
  }

  $source = array($_SESSION['login'],$_POST['subject'],$_POST['mail']);

  $file = fopen("../txt/md5.file.txt", "a+");
  $resource = array();
  while ($a = fgets($file)) {
    $resource[] = trim($a);
  }

  if ($_FILES['file']['size'] !== 0) {
    if ($_FILES['file']['error'] == UPLOAD_ERR_OK) { // condition for check file upload or not

      $directory = "../file"; // directory of saving users image
      $files = scandir($directory); // scan flie folder and put it in an array
      array_shift($files); // delete first element of array
      array_shift($files); // delete first element of array
      $num_files = count($files); // count directory and put it in array
      $filetomove = $_FILES["file"]["tmp_name"]; // address of uploaded file
      $md5file = md5_file($filetomove);
      if (in_array($md5file,$resource)) {
        $message_error[2] = "!This is a duplicate file. Please upload another one";
        fclose($file);
      }
      else {

        fwrite($file,$md5file);
        fwrite($file,PHP_EOL);
        fclose($file);
        if (in_array($_FILES["file"]["name"],$files)) { // condition for check the file exist in directory or not

          $destination = "../file/".$num_files.$_FILES["file"]["name"]; // address for where we want to save the file
          $source['file_adrres'] = $destination; // put address in source array
          move_uploaded_file($filetomove,$destination); // move uploaded file to our directory
          }
          else {

            $filetomove = $_FILES["file"]["tmp_name"]; // address of uploaded file
            $destination = "../file/".$_FILES["file"]["name"]; // address for where we want to save the file
            $source['file'] = $destination; // put address in source array
            move_uploaded_file($filetomove,$destination); // move uploaded file to our directory
            $source['file_adrres'] = $destination; // put address in source array
            }
        }
      }
      else {

        $message_error[0][5] = "!Something went wrong in upload file. Please try again";
      }
  }

  if (isset($message_error[0])) {
    return $message_error;
  }

  if (empty($_POST['recipients'])) {
    if (isset($source['file_adrres'])) {

      $file = $source['file_adrres'];
      querySQL("INSERT INTO `mail`(`idmail`, `subject`, `text`, `file`, `date`, `time`, `seen`) VALUES ('','$source[1]','$source[2]','$file',CURRENT_DATE,CURRENT_TIME,'0')");
      $iduser = mysqli_fetch_row(querySQL("select idusers from users where '$source[0]' = users_username"));
      $idmail = mysqli_fetch_row(querySQL("select idmail from mail where subject = '$source[1]' and text = '$source[2]'"));

      querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$iduser[0]','$idmail[0]','')");
      querySQL("INSERT INTO `darft`(`iddarft`, `users_has_mail_users_idusers`, `users_has_mail_mail_idmail`) VALUES ('','$iduser[0]','$idmail[0]')");
      $message_error[1] = "!Mail save to draft";
      return $message_error;
    }
    else {

      querySQL("INSERT INTO `mail`(`idmail`, `subject`, `text`, `file`, `date`, `time`, `seen`) VALUES ('','$source[1]','$source[2]','0',CURRENT_DATE,CURRENT_TIME,'0')");
      $iduser = mysqli_fetch_row(querySQL("select idusers from users where '$source[0]' = users_username"));
      $idmail = mysqli_fetch_row(querySQL("select idmail from mail where subject = '$source[1]' and text = '$source[2]'"));

      querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$iduser[0]','$idmail[0]','')");
      querySQL("INSERT INTO `darft`(`iddarft`, `users_has_mail_users_idusers`, `users_has_mail_mail_idmail`) VALUES ('','$iduser[0]','$idmail[0]')");
      $message_error[1] = "!Mail save to draft";
      return $message_error;
    }
  }
  else {
    $recipients = explode("&",$_POST['recipients']);
    $arrusers = mysqli_fetch_all(querySQL("select users_username from users where '$source[0]' != users_username"));
    foreach ($arrusers as $username) {
      $allusers[$username[0]] = $username[0];
    }

    foreach ($recipients as $username) {
      if (in_array($username,$allusers)) {

        if (isset($source['file_adrres'])) {

          $file = $source['file_adrres'];
          $idgiver = mysqli_fetch_row(querySQL("select idusers from users where '$username' = users_username"));
          querySQL("INSERT INTO `mail`(`idmail`, `subject`, `text`, `file`, `date`, `time`, `mail_idgiver`, `seen`) VALUES ('','$source[1]','$source[2]','$file',CURRENT_DATE,CURRENT_TIME,'$idgiver[0]','0')");
          $idsender = mysqli_fetch_row(querySQL("select idusers from users where '$source[0]' = users_username"));
          $idmail = mysqli_fetch_row(querySQL("select idmail from mail where subject = '$source[1]' and text = '$source[2]' and mail_idgiver = '$idgiver[0]'"));

          querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$idsender[0]','$idmail[0]','$idgiver[0]')");
          $message_error[1] = "!Mail has been sent";
        }
        else {

          $idgiver = mysqli_fetch_row(querySQL("select idusers from users where '$username' = users_username"));
          querySQL("INSERT INTO `mail`(`idmail`, `subject`, `text`, `file`, `date`, `time`, `mail_idgiver`, `seen`) VALUES ('','$source[1]','$source[2]','0',CURRENT_DATE,CURRENT_TIME,'$idgiver[0]','0')");
          $idsender = mysqli_fetch_row(querySQL("select idusers from users where '$source[0]' = users_username"));
          $idmail = mysqli_fetch_row(querySQL("select idmail from mail where subject = '$source[1]' and text = '$source[2]' and mail_idgiver = '$idgiver[0]'"));

          querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$idsender[0]','$idmail[0]','$idgiver[0]')");
          $message_error[1] = "!Mail has been sent";
        }
      }
      else {
        if (isset($source['file_adrres'])) {

          $file = $source['file_adrres'];
          querySQL("INSERT INTO `mail`(`idmail`, `subject`, `text`, `file`, `date`, `time`, `seen`) VALUES ('','$source[1]','$source[2]','$file',CURRENT_DATE,CURRENT_TIME,'0')");
          $iduser = mysqli_fetch_row(querySQL("select idusers from users where '$source[0]' = users_username"));
          $idmail = mysqli_fetch_row(querySQL("select idmail from mail where subject = '$source[1]' and text = '$source[2]'"));

          querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$iduser[0]','$idmail[0]','')");
          querySQL("INSERT INTO `darft`(`iddarft`, `users_has_mail_users_idusers`, `users_has_mail_mail_idmail`) VALUES ('','$iduser[0]','$idmail[0]')");
          $message_error[1] = "!Mail save to draft";
        }
        else {

          querySQL("INSERT INTO `mail`(`idmail`, `subject`, `text`, `file`, `date`, `time`, `seen`) VALUES ('','$source[1]','$source[2]','0',CURRENT_DATE,CURRENT_TIME,'0')");
          $iduser = mysqli_fetch_row(querySQL("select idusers from users where '$source[0]' = users_username"));
          $idmail = mysqli_fetch_row(querySQL("select idmail from mail where subject = '$source[1]' and text = '$source[2]'"));

          querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$iduser[0]','$idmail[0]','')");
          querySQL("INSERT INTO `darft`(`iddarft`, `users_has_mail_users_idusers`, `users_has_mail_mail_idmail`) VALUES ('','$iduser[0]','$idmail[0]')");
          $message_error[1] = "!Mail save to draft";
        }
      }
    }
  }
  if (isset($message_error)) {
    return $message_error;
  }
}

# get details of sent messages from database
function sent():array
{
  $username = $_SESSION["login"];
  $detail[0] = mysqli_fetch_all(querySQL("select subject , date , users_username , idmail from mail , users , users_has_mail where seen = 0 and idusers = idgiver and users_idusers = (select idusers from users where users_username = '$username') and idmail = mail_idmail"));
  $detail[1] = mysqli_fetch_all(querySQL("select subject , date , users_username , idmail , seen from mail , users , users_has_mail where seen = 1 and idusers = idgiver and users_idusers = (select idusers from users where users_username = '$username') and idmail = mail_idmail"));
  return $detail;
}

# get details of one draft message from database
function showsentmail():array
{
  $username = $_SESSION["login"];
  $idmail = $_GET['idmail'];
  $detail = mysqli_fetch_all(querySQL("select subject , date , text , time , file , users_username , idmail from mail inner join users_has_mail on mail_idmail = idmail INNER join users on users_idusers != idusers where idusers = idgiver and users_idusers = (select idusers from users where users_username = '$username') and idmail = '$idmail'"));
  return $detail;
}

# get details of inbox messages from database
function inbox():array
{
  $username = $_SESSION["login"];
  $detail = mysqli_fetch_all(querySQL("select subject , date , users_username , idmail from mail INNER join users_has_mail on idmail = mail_idmail inner join users on users_idusers = idusers and idgiver = (select idusers from users where '$username' = users_username)"));
  return $detail;
}

# get details of one inbox message from database
function showinboxmail()
{
  $username = $_SESSION["login"];
  $idmail = $_GET['idmail'];
  querySQL("UPDATE `mail` SET `seen`= 1 WHERE '$idmail' = idmail");
  $detail = mysqli_fetch_all(querySQL("select subject , date , text , time , file , users_username , idmail from mail INNER join users_has_mail on idmail = mail_idmail inner join users on users_idusers = idusers and idgiver = (select idusers from users where '$username' = users_username) and idmail = '$idmail'"));
  return $detail;
}

# get details of sent messages from database
function draft()
{
  $username = $_SESSION["login"];
  $detail = mysqli_fetch_all(querySQL("SELECT subject , date , idmail FROM mail INNER join users_has_mail ON mail_idmail = idmail INNER JOIN users ON	users.idusers = users_has_mail.users_idusers WHERE users_has_mail.idgiver = 0 AND users.users_username = '$username'"));
  return $detail;
}

# get details of one draft message from database
function darftmail()
{
  $idmail = $_GET['idmail'];
  $detail = mysqli_fetch_all(querySQL("SELECT subject, text , idmail FROM mail INNER join users_has_mail ON mail_idmail = idmail INNER JOIN users ON	users.idusers = users_has_mail.users_idusers WHERE users_has_mail.idgiver = 0 AND idmail = '$idmail'"));
  return $detail;
}

function compose():array
{
  if (isset($_GET['idmail'])) {
    $idmail = $_GET['idmail'];
    $username = $_SESSION["login"];

    if (empty($_POST['recipients'])) {
      return send();
    }
    else {

      querySQL("DELETE FROM `darft` WHERE users_has_mail_mail_idmail = $idmail");
      $iduser = mysqli_fetch_row(querySQL("select idusers from users where '$username' = users_username"));
      querySQL("DELETE FROM `users_has_mail` WHERE mail_idmail = '$idmail' and idgiver = 0 and users_idusers = '$iduser[0]'");

      $recipients = explode("&",$_POST['recipients']);
      $arrusers = mysqli_fetch_all(querySQL("select users_username from users where '$username' != users_username"));
      foreach ($arrusers as $user) {
        $allusers[$user[0]] = $user[0];
      }
      foreach ($recipients as $user) {
        if (in_array($user,$allusers)) {
          $idgiver = mysqli_fetch_row(querySQL("select idusers from users where '$user' = users_username"));
          querySQL("INSERT INTO `users_has_mail`(`users_idusers`, `mail_idmail`, `idgiver`) VALUES ('$iduser[0]','$idmail','$idgiver[0]')");
          $message_error[1] = "!Mail has been sent";
        }
        else {
          $message_error = send();
        }
    }
    if (isset($message_error)) {
      return $message_error;
    }
    }
  }
  else {
    return send();
  }
}

function profile():array
{
  $username = $_SESSION["login"];
  $detail = mysqli_fetch_all(querySQL("select users_username , users_email from users where users_username = '$username' "));
  return $detail;
}

function search()
{
  $value = $_GET["mailserach"];
  $username = $_SESSION["login"];
  $iduser = mysqli_fetch_row(querySQL("select idusers from users where '$username' = users_username"));
  $detail = mysqli_fetch_all(querySQL("select subject , date , idmail from mail inner join users_has_mail on mail_idmail = idmail and (users_idusers = '$iduser[0]' or idgiver = '$iduser[0]') WHERE text like '%$value%'"));
  return $detail;
}

function showMailSearch()
{
  $idmail = $_GET['idmail'];
  $detail = mysqli_fetch_all(querySQL("select subject , date , text , time , file , idmail from mail inner join users_has_mail on mail_idmail = idmail INNER JOIN users ON users_idusers = idusers WHERE idmail = '$idmail'"));
  return $detail;
}

function fliterFile():array
{
  $username = $_SESSION["login"];
  $detail = mysqli_fetch_all(querySQL("select subject , date , users_username , idmail , file from mail INNER join users_has_mail on idmail = mail_idmail  inner join users on users_idusers = idusers and idgiver = (select idusers from users where '$username' = users_username) where file != '0'"));
  return $detail;
}

function fliterFileSent():array
{
  $username = $_SESSION["login"];
  $detail[0] = mysqli_fetch_all(querySQL("select subject , date , users_username , idmail from mail , users , users_has_mail where seen = 0 and idusers = idgiver and users_idusers = (select idusers from users where users_username = '$username') and idmail = mail_idmail and file != '0'"));
  $detail[1] = mysqli_fetch_all(querySQL("select subject , date , users_username , idmail , seen from mail , users , users_has_mail where seen = 1 and idusers = idgiver and users_idusers = (select idusers from users where users_username = '$username') and idmail = mail_idmail and file != '0'"));
  return $detail;
}
?>
